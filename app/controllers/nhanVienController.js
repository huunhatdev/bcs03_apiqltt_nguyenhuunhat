const renderDSND = () => {
  showLoading();
  QLTTServ.getDS()
    .then((res) => {
      viewTable(res.data);
    })
    .catch((err) => console.log(err));
  hideLoading();
};

const viewTable = (arr) => {
  let contentHTML = "";
  arr.forEach((data, i) => {
    contentHTML += `<tr>
          <td>${i + 1}</td>
          <td>${data.taiKhoan}</td>
          <td>${data.matKhau}</td>
          <td>${data.hoTen}</td>
          <td>${data.email}</td>
          <td>${data.ngonNgu}</td>
          <td>${data.loaiND ? "Giảng viên" : "Sinh viên"}</td>
          <td>
            <button class="btn btn-primary " onclick="btnEditNV(${
              data.id
            })">Sửa</button>
            <button class="btn btn-danger " onclick="deleteNV('${
              data.id
            }')">Xoá</button>
          </td>
        </tr>`;
  });

  document.getElementById("tblDanhSachNguoiDung").innerHTML = contentHTML;
};
const resetForm = () => {
  document.getElementById("form-modal").reset();
};

//loading
const showLoading = () => {
  document.getElementById("loading").style.display = "flex";

  console.log("show");
};
const hideLoading = () => {
  setTimeout(() => {
    document.getElementById("loading").style.display = "none";
  }, 2000);
};

const getValueForm = () => {
  let taiKhoan = document.getElementById(`TaiKhoan`).value;
  let hoTen = document.getElementById(`HoTen`).value;
  let matKhau = document.getElementById(`MatKhau`).value;
  let email = document.getElementById(`Email`).value;
  let hinhAnh = document.getElementById(`HinhAnh`).value;
  let loaiNguoiDung = document.getElementById(`loaiNguoiDung`).value;
  let loaiNgonNgu = document.getElementById(`loaiNgonNgu`).value;
  let moTa = document.getElementById(`MoTa`).value;

  return (nhanVien = new NhanVien(
    taiKhoan,
    hoTen,
    matKhau,
    email,
    hinhAnh,
    loaiNguoiDung,
    loaiNgonNgu,
    moTa
  ));
};

const btnEditNV = (id) => {
  QLTTServ.getId(id)
    .then((res) => {
      console.log(res);
      document.getElementById(`txtId`).value = res.data.id;
      document.getElementById(`TaiKhoan`).value = res.data.taiKhoan;
      document.getElementById(`HoTen`).value = res.data.hoTen;
      document.getElementById(`MatKhau`).value = res.data.matKhau;
      document.getElementById(`Email`).value = res.data.email;
      document.getElementById(`HinhAnh`).value = res.data.hinhAnh;
      document.getElementById(`loaiNguoiDung`).value = res.data.loaiND
        ? "0"
        : "1";
      document.getElementById(`loaiNgonNgu`).value = res.data.ngonNgu;
      document.getElementById(`MoTa`).value = res.data.moTa;
      // resetForm();
      document.querySelector("#btnAdd").style.display = "none";
      document.querySelector("#btnUpdate").style.display = "block";
      $("#myModal").modal("show");
    })
    .catch((err) => console.log(err));
};

const deleteNV = (id) => {
  console.log("del");
  showLoading();
  QLTTServ.deleteNV(id)
    .then((res) => {
      renderDSND();
    })
    .catch((err) => console.log(err));
  hideLoading();
};

const updateNV = () => {
  if (checkValidate()) {
    showLoading();
    let dataNV = getValueForm();
    console.log("updale...");
    let id = document.getElementById(`txtId`).value;
    QLTTServ.getId(id)
      .then((res) => {
        if (res.data.id === id) {
          QLTTServ.updateNV(id, dataNV)
            .then((res) => {
              console.log("render...");
              renderDSND();
              $("#myModal").modal("hide");
            })
            .catch((err) => {
              console.log(err);
            });
        }
      })
      .catch((err) => {
        console.log(err);
      });
    hideLoading();
  }
};
