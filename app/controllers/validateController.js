const checkValidate = () => {
  const validate = new Validate();
  if (
    validate.kiemTaiKhoan(
      "TaiKhoan",
      "notiTaiKhoan",
      "Tên tài khoản không hợp lệ"
    ) &
    validate.kiemTen("HoTen", "notiHoTen", "Tên không hợp lệ") &
    validate.kiemMatKhau(
      "MatKhau",
      "notiMatKhau",
      "Mật khẩu bao gồm chữ thường, in, số, kí tư đặc biệt, 6-8 kí tự"
    ) &
    validate.kiemTraEmail(
      "Email",
      "notiEmail",
      "Tài khoản email không hợp lệ"
    ) &
    validate.checkNull(
      "HinhAnh",
      "notiHinhAnh",
      "Hình ảnh không được để trống"
    ) &
    validate.checkNull(
      "loaiNguoiDung",
      "notiLoaiNguoiDung",
      "Loại người dùng không được để trống"
    ) &
    validate.checkNull(
      "loaiNgonNgu",
      "notiLoaiNgonNgu",
      "Loại ngôn ngữ không được để trống"
    ) &
    validate.kiemTraMota(
      "MoTa",
      "notiMoTa",
      "Mô tả dài 60 ký tự và không được để trống"
    )
  ) {
    return true;
  }
  return false;
};
