arrDs = [];
renderDSND();

//Thêm
document.getElementById("btnAdd").addEventListener("click", () => {
  if (checkValidate()) {
    showLoading();
    let newNV = getValueForm();
    QLTTServ.getInfo(newNV.taiKhoan)
      .then((res) => {
        document.getElementById("notiTaiKhaon").innerText =
          "Tài khoản đã tồn tại";
      })
      .catch((err) => {
        document.getElementById("notiTaiKhoan").innerText = "";
        QLTTServ.addNV(newNV)
          .then((res) => {
            $("#myModal").modal("hide");
            renderDSND();
          })
          .catch((err) => {
            console.log(err);
          });
      });
    hideLoading();
  }
});

//thêm người dùng
document.getElementById("btnThemNguoiDung").addEventListener("click", () => {
  document.querySelector("#btnAdd").style.display = "block";
  document.querySelector("#btnUpdate").style.display = "none";
  resetForm();
});

//btn update
document.getElementById("btnUpdate").addEventListener("click", () => {
  updateNV();
});
//btn reset
document.getElementById("btnReset").addEventListener("click", () => {
  resetForm();
});

//btn  close
document.getElementById("btnExit").addEventListener("click", () => {
  resetForm();
  $("#myModal").modal("hide");
});
