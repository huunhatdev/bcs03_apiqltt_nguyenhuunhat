const BASE_URL =
  "https://6271e18225fed8fcb5ec0cef.mockapi.io/quan-ly-trung-tam";
const QLTTServ = {
  getDS: function () {
    return axios({
      url: `${BASE_URL}`,
      method: "GET",
    });
  },
  getId: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },
  getInfo: function (taiKhoan) {
    return axios({
      url: `${BASE_URL}?taiKhoan=${taiKhoan}`,
      method: "GET",
    });
  },
  addNV: function (data) {
    return axios({
      url: `${BASE_URL}`,
      method: "POST",
      data: data,
    });
  },
  updateNV: function (id, data) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: data,
    });
  },
  deleteNV: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
};
