const Validate = function () {
  this.kiemTaiKhoan = function (_idTarget, _idError, _errNoti) {
    let value = document.getElementById(_idTarget).value;
    let pattern = /^([a-zA-Z0-9\_]+)$/;
    if (pattern.test(value)) {
      document.getElementById(_idError).innerText = ``;
      return true;
    }
    document.getElementById(_idError).innerText = _errNoti;
    return false;
  };
  this.kiemTen = function (_idTarget, _idError, _errNoti) {
    let value = document.getElementById(_idTarget).value;
    let pattern =
      /^([A-VXYỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐa-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+)((\s{1}[A-VXYỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐa-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+){1,})$/;
    if (pattern.test(value)) {
      document.getElementById(_idError).innerText = ``;
      return true;
    }
    document.getElementById(_idError).innerText = _errNoti;
    return false;
  };
  this.kiemMatKhau = function (_idTarget, _idError, _errNoti) {
    let value = document.getElementById(_idTarget).value;
    let pattern =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,8}$/;
    if (pattern.test(value)) {
      document.getElementById(_idError).innerText = ``;
      return true;
    }
    document.getElementById(_idError).innerText = _errNoti;
    return false;
  };
  this.kiemTraEmail = function (_idTarget, _idError, _errNoti) {
    let value = document.getElementById(_idTarget).value;
    let pattern = /^[\w]+([\.-]?[\w]+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$$/;
    if (pattern.test(value)) {
      document.getElementById(_idError).innerText = ``;
      return true;
    }
    document.getElementById(_idError).innerText = _errNoti;
    return false;
  };
  this.checkNull = function (_idTarget, _idError, _errNoti) {
    let value = document.getElementById(_idTarget).value;
    if (value) {
      document.getElementById(_idError).innerText = ``;
      return true;
    }
    console.log(_idError);
    document.getElementById(_idError).innerText = _errNoti;
    return false;
  };
  this.kiemTraMota = function (_idTarget, _idError, _errNoti) {
    let value = document.getElementById(_idTarget).value;
    if (value.length > 0 && value.length <= 60) {
      document.getElementById(_idError).innerText = ``;
      return true;
    }
    console.log({ _idError, _errNoti });
    document.getElementById(_idError).innerText = _errNoti;
    return false;
  };
};
