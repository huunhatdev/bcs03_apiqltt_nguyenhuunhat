const NhanVien = function (
  _taiKhoan,
  _hoTen,
  _matKhau,
  _email,
  _hinhAnh,
  _loaiNguoiDung,
  _loaiNgonNgu,
  _moTa
) {
  this.taiKhoan = _taiKhoan;
  this.hoTen = _hoTen;
  this.matKhau = _matKhau;
  this.email = _email;
  this.hinhAnh = _hinhAnh;
  this.loaiND = _loaiNguoiDung === "0" ? true : false;
  this.ngonNgu = _loaiNgonNgu;
  this.moTa = _moTa;
};
